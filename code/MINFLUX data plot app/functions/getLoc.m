function xyz = getLoc (app) % public
    xyz = app.xyz(app.ftr, :);
end